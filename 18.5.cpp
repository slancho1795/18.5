#include <iostream>
#include <string>
class Player
{
public:
    std::string Name;
    int Scope;

    Player(std::string _name = "Anonim", int _scope = 0) : Name(_name), Scope(_scope)
    {
    }
};

void BubbleSort(Player* ArrayPlayers, int CountPlayers)
{
    bool b = true;
    while (b)
    {
        b = false;
        for (int i = 0; i < CountPlayers - 1; i++)
        {
            if (ArrayPlayers[i].Scope < ArrayPlayers[i + 1].Scope)
            {
                std::swap(ArrayPlayers[i], ArrayPlayers[i + 1]);
                b = true;
            }
        }
    }
}

void SetPlayerInfo(Player* ArrayPlayers, int CountPlayers)
{
    for (int i = 0; i < CountPlayers; ++i)
    {
        std::cout << std::endl << std::endl << "Enter player details!\nName: ";
        std::cin >> ArrayPlayers[i].Name;
        std::cout << "Scope: ";
        std::cin >> ArrayPlayers[i].Scope;
    }
}

int main()
{
    int CountPlayers;

    std::cout << "Enter number of players: ";
    std::cin >> CountPlayers;
    auto ArrayPlayers = new Player[CountPlayers];

    SetPlayerInfo(ArrayPlayers, CountPlayers);

    BubbleSort(ArrayPlayers, CountPlayers);

    std::cout << "\n\nLeaderboard\n";
    for (int i = 0; i < CountPlayers; ++i)
    {
        std::cout << std::endl << i + 1 << " " << ArrayPlayers[i].Name << " " << ArrayPlayers[i].Scope;
    }

    delete[] ArrayPlayers;
}